"use strict";

let decoder = {};

decoder.symbolMap = {
  '@': '"',
  '#': '№',
  '$': ';',
  '^': ':',
  '|': '/',
  '&': '?'
};

decoder.lettersMap = {
  'q': 'й',
  'w': 'ц',
  'e': 'у',
  'r': 'к',
  't': 'е',
  'y': 'н',
  'u': 'г',
  'i': 'ш',
  'o': 'щ',
  'p': 'з',
  '[': 'х',
  '{': 'Х',
  ']': 'ъ',
  '}': 'Ъ',
  'a': 'ф',
  's': 'ы',
  'd': 'в',
  'f': 'а',
  'g': 'п',
  'h': 'р',
  'j': 'о',
  'k': 'л',
  'l': 'д',
  ';': 'ж',
  ':': 'Ж',
  "'": 'э',
  '"': 'Э',
  'z': 'я',
  'x': 'ч',
  'c': 'с',
  'v': 'м',
  'b': 'и',
  'n': 'т',
  'm': 'ь',
  ',': 'б',
  '<': 'Б',
  '.': 'ю',
  '>': 'Ю',
  '/': '.',
  '?': ','
};

decoder.decode = function(str) {
  let _self = this;
  let fullMap = Object.assign(_self.symbolMap, _self.lettersMap);

  function isInMap(symbol) {
    let ret = false;

    return ( fullMap[symbol.toLowerCase()]) ? true : false;
  }

  let decodedStr = "";
  for(let i=0; i < str.length; i++) {
    let symbol = str[i];

    if ( isInMap(symbol) ) {
      if ( fullMap[symbol] ) {
        decodedStr += fullMap[symbol];
      } else {
        decodedStr += fullMap[symbol.toLowerCase()].toUpperCase();
      }
    } else {
      decodedStr += symbol;
    }
    
  }

  return decodedStr;
};

module.exports = decoder;