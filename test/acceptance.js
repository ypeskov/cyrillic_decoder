"use strict";

let should = require("should");
let modulePath = "../index";

describe("Check convert", () => {
  let _decoder = null;

  before(done => {
    delete require.cache[require.resolve(modulePath)];
    _decoder = require(modulePath);
    done();
  });

  it("decoder should exist", done => {
    should.exist(_decoder);
    done();
  });

  it("should convert small letters", done => {
    let str = "Dctv ghbdtn! Lj,hj gj;fkjdfnm d yfi e.nysq xfn ^)";
    let decodedGoal = "Всем привет! Добро пожаловать в наш уютный чат :)";

    let decodedStr = _decoder.decode(str);
    decodedStr.should.eql(decodedGoal);
    done();
  });
});